module Main exposing (..)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (main_)
import Html.Attributes as HtmlAttr
import List.Extra as Lst



-- MAIN


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { input : String
    , base : String
    }


init : Model
init =
    { input = "340"
    , base = "8"
    }



-- UPDATE


type Msg
    = Input String
    | Base String


update : Msg -> Model -> Model
update msg model =
    case msg of
        Input val ->
            if String.toInt val /= Nothing || val == "" then
                { model | input = val }

            else
                model

        Base val ->
            if String.toInt val /= Nothing || val == "" then
                { model | base = val }

            else
                model


view model =
    layout [ height fill ] <|
        column [ spacing 30, centerX, paddingXY 20 30, height fill ]
            [ el [ centerX ] <| text "Fast Powering Mod"
            , commands model
            , settings model
            , showCalculation model
            , link [ alignBottom, centerX, Font.size 10, Font.color <| rgb 0 0 0.5 ]
                { url = "https://f0i.de/impressum"
                , label = text "impressum"
                }
            ]


commands : Model -> Element Msg
commands model =
    wrappedRow
        [ spacing 10, centerX ]
        [ Input.text [ width <| px 100 ]
            { onChange = Base
            , text = model.base
            , placeholder = Just <| Input.placeholder [] <| text "2"
            , label = Input.labelRight [] (text "")
            }
        , text " to the power of "
        , Input.text [ width <| px 100 ]
            { onChange = Input
            , text = model.input
            , placeholder = Just <| Input.placeholder [] <| text "1"
            , label = Input.labelRight [] (text "")
            }
        , text <|
            " (mod "
                ++ iToS (model.input |> String.toInt |> Maybe.withDefault 2 |> (+) 1)
                ++ ")"
        ]


settings : Model -> Element msg
settings _ =
    column [ spacing 10, centerX ]
        []


btn : String -> msg -> Element msg
btn label msg =
    Input.button []
        { onPress = Just msg
        , label =
            el
                [ Border.width 1
                , mouseDown [ Background.color <| rgb 0.8 0.8 0.8 ]
                , paddingXY 5 10
                , Border.rounded 4
                ]
            <|
                text label
        }


iToS : Int -> String
iToS =
    String.fromInt


takeRight : Int -> List a -> List a
takeRight n list =
    list |> List.reverse |> List.take n |> List.reverse


dropRight : Int -> List a -> List a
dropRight n list =
    list |> List.reverse |> List.drop n |> List.reverse


isEven : Int -> Bool
isEven n =
    n |> modBy 2 |> (==) 0


showCalculation : Model -> Element msg
showCalculation model =
    let
        base =
            model.base |> String.toInt |> Maybe.withDefault 1

        power =
            model.input |> String.toInt |> Maybe.withDefault 1

        mod =
            power + 1

        parts =
            split power

        ( result, hasNonPrimSqrt ) =
            calculateSolution base base mod (parts |> List.reverse)
    in
    column [ spacing 20, Font.family [ Font.monospace ] ] <|
        [ column [] (parts |> List.map (showPart base) |> List.map text)
        , text <| String.repeat 20 "-"
        , showSolution base base mod (parts |> List.reverse)
        , text <| String.repeat 20 "-"
        , text <| showPower base power ++ " = " ++ iToS result ++ " (mod " ++ iToS mod ++ ")"
        , text <|
            iToS mod
                ++ (case ( result, hasNonPrimSqrt ) of
                        ( 1, False ) ->
                            " might be a prime because no Non-Trivial-Sqrt was found"

                        ( 1, True ) ->
                            " is not prime because it has a Non-Trivial-Sqrt"

                        ( _, False ) ->
                            " is not a prime because the result is not 1"

                        ( _, True ) ->
                            " is not a prime because both the result is not 1 and a Non-Trivial-Sqrt was found"
                   )
        ]


split : Int -> List ( Int, Bool )
split power =
    if power < 2 then
        []

    else if isEven power then
        ( power, True ) :: split (power // 2)

    else
        ( power, False ) :: split (power - 1)


showPower : Int -> Int -> String
showPower base power =
    if power > 1 then
        iToS base
            ++ " ^ "
            ++ iToS power

    else
        iToS base


showPart : Int -> ( Int, Bool ) -> String
showPart base ( power, squared ) =
    showPower base power
        ++ " = "
        ++ (if squared then
                "("
                    ++ showPower base (power // 2)
                    ++ ")"
                    ++ " ^ 2 "

            else
                "("
                    ++ showPower base (power - 1)
                    ++ ")"
                    ++ " * "
                    ++ iToS base
           )


showSolution : Int -> Int -> Int -> List ( Int, Bool ) -> Element msg
showSolution acc base mod list =
    case list of
        ( power, True ) :: xs ->
            let
                product =
                    acc * acc

                result =
                    product |> modBy mod

                isNonTrivSqrt =
                    acc /= 1 && acc /= mod - 1 && result == 1

                formula =
                    showPart base ( power, True )
                        ++ "  -->  "
                        ++ showPower acc 2
                        ++ " = "
                        ++ (iToS product ++ " -> " ++ iToS result)
                        ++ (" (mod " ++ iToS mod ++ ")")

                offset =
                    String.length formula - String.length (iToS mod) - 8
            in
            column []
                [ text <| formula
                , if isNonTrivSqrt then
                    text (String.repeat offset " " ++ "'--> non trivial sqrt")

                  else
                    none
                , showSolution result base mod xs
                ]

        ( power, False ) :: xs ->
            let
                product =
                    acc * base

                result =
                    product |> modBy mod
            in
            column []
                [ text <|
                    showPart base ( power, False )
                        ++ "  -->  "
                        ++ (iToS acc ++ " * " ++ iToS base)
                        ++ " = "
                        ++ (iToS product ++ " -> " ++ iToS result)
                        ++ (" (mod " ++ iToS mod ++ ")")
                , showSolution result base mod xs
                ]

        _ ->
            none


calculateSolution : Int -> Int -> Int -> List ( Int, Bool ) -> ( Int, Bool )
calculateSolution acc base mod list =
    case list of
        ( power, True ) :: xs ->
            let
                product =
                    acc * acc

                result =
                    product |> modBy mod

                isNonTrivSqrt =
                    acc /= 1 && acc /= mod - 1 && result == 1

                ( a, b ) =
                    calculateSolution result base mod xs
            in
            ( a, b || isNonTrivSqrt )

        ( power, False ) :: xs ->
            let
                product =
                    acc * base

                result =
                    product |> modBy mod

                ( a, b ) =
                    calculateSolution result base mod xs
            in
            ( a, b )

        _ ->
            ( acc, False )
