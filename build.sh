#!/usr/bin/env bash

# usage ./build.sh <ssh-host>

set -x

elm make src/Main.elm --optimize --output=index.html \
  || exit $?

if [[ $# -gt 0 ]] ; then
  scp index.html $1:/var/www/elm.f0i.de/powermod/
fi

